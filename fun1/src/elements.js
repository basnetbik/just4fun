import shortid from 'shortid';


export class Santa {
  constructor(canvas, x=100, y=120) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext('2d');

    this.santa = new Image();
    this.images = [];
    for (let i=0; i<16; i++) {
      this.images.push(
        require(`./static/images/santa/santa${i}.png`)
      )
    }
    // width and height of image
    this.w = 40;
    this.h = 43;

    this.yOrigin = y;
    this.xOrigin = x;

    this.jumpHeight = 80;

    this.resetPosition(x, y);
  }

  resetPosition = () => {
    this.x = this.xOrigin;
    this.y = this.yOrigin;

    this.jump = false;
    this.fall = false;
    this.jumped = false;

    this.position = 0;
  };

  move = () => {
    if (this.fall === true) {
      if (this.y < this.canvas.height)
        this.y += 5;
      else {
        this.resetPosition();
      }
    } else if (this.jump === true) {
      if (this.y > this.yOrigin-this.jumpHeight && this.jumped === false)
        this.y -= 5;
      else if (this.y < this.yOrigin) {
        this.jumped = true;
        this.y += 5
      } else {
        this.jump = false;
        this.jumped = false;
      }
    }
  };

  render() {
    this.santa.src = this.images[this.position];
    this.santa.onload = () => {
      this.ctx.drawImage(this.santa, this.x, this.y);
    };

    this.position = (this.position + 1) % 16;
  }
}


class Gift {
  constructor(canvas, y) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext('2d');
    this.x = this.canvas.width;
    this.y = y;

    this.gift = new Image();
    this.image = require('./static/images/gift.png');
    // width and height of image
    this.w = 20;
    this.h = 20;
  }

  move = (speed=10) => {
    this.x -= speed;
  };

  hitTest = (santa) => {
    // check if hits santa
    return !(
      (this.x + this.w < santa.x) || (this.x > santa.x + santa.w) ||
      (this.y + this.h < santa.y) || (this.y > santa.y + santa.h)
    );
  };

  render = () => {
    this.gift.src = this.image;
    this.gift.onload = () => {
      this.ctx.drawImage(this.gift, this.x, this.y);
    }
  }
}

export class Gifts {
  constructor(canvas) {
    this.canvas = canvas;

    this.gifts = {};
    this.giftPositions = [125, 80];
    this.speed = 10;

    this.addGift();
  }

  increaseSpeed = () => {
    this.speed = 30;
  };

  reduceSpeed = () => {
    this.speed = 5;
  };

  resetSpeed = () => {
    this.speed = 10;
  };

  addGift = () => {
    const pos = Math.floor(Math.random()*2);
    const gift = new Gift(this.canvas, this.giftPositions[pos]);
    this.gifts[shortid.generate()] = gift;
    this.last = gift;
  };

  removeGift = (id) => {
    delete this.gifts[id];
  };

  render(santa) {
    // gap between last gift and new gift
    const gap = 100 + Math.floor(Math.random()*50);
    if (this.last.x < this.canvas.width-gap)
      // add gift
      this.addGift();

    let remove = [];
    let score = 0;

    for(let gift in this.gifts) {
      // move gift
      this.gifts[gift].move(this.speed);

      // render gift
      this.gifts[gift].render();

      // check if hits santa
      if (this.gifts[gift].hitTest(santa) === true) {
        score += 1;

        // if gift hits santa, remove gift
        remove.push(gift);
      }

      // if gift gets out of canvas remove it
      if (this.gifts[gift].x < 0)
        remove.push(gift)
    }

    // remove gifts that hit santa or out of bound of canvas window
    for (let i=0; i<remove.length; i++)
      this.removeGift(remove[i]);

    return score;
  }
}


class Gap {
  constructor(canvas) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext('2d');
    this.x = this.canvas.width;
    this.y = 155;
    this.w = 100+Math.floor(Math.random()*100);
    this.h = 52;
  }

  move = (speed=10) => {
    this.x -= speed;
  };

  hitTest = (santa) => {
    const threshold = 40;
    return !(
      (this.x + this.w - threshold < santa.x) || (this.x + threshold > santa.x + santa.w)
      || (this.y + 52 < santa.y) || (this.y > santa.y + santa.h)
    );
  };

  render = () => {
    this.ctx.fillStyle = 'blue';
    this.ctx.fillRect(this.x, this.y, this.w, this.h);
  }
}

export class Gaps {
  constructor(canvas) {
    this.canvas = canvas;

    this.gaps = {};
    this.speed = 10;

    this.addGap()
  }

  increaseSpeed = () => {
    this.speed = 30;
  };

  reduceSpeed = () => {
    this.speed = 5;
  };

  resetSpeed = () => {
    this.speed = 10;
  };

  addGap = () => {
    const gap = new Gap(this.canvas);
    this.gaps[shortid.generate()] = gap;
    this.last = gap;
  };

  render(santa) {
    // gap between last gap and new gap
    const gap = 500 + Math.floor(Math.random()*50);
    if (this.last.x < this.canvas.width-gap)
      this.addGap();

    let remove = [];
    let fell = false;

    for(let gap in this.gaps) {
      // move gap
      this.gaps[gap].move(this.speed);

      // render gap
      this.gaps[gap].render();

      // check if hits santa
      if (this.gaps[gap].hitTest(santa) === true) {
        fell = true;
      }

      // check if gets out of canvas
      if (this.gaps[gap].x < -this.gaps[gap].w-10)
        remove.push(gap)
    }

    // remove gaps that get out of bound of canvas window
    for (let i=0; i<remove.length; i++)
      delete this.gaps[remove[i]];

    return fell;
  }
}
