import React from 'react';
import './App.css';
import Game from "./Game";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      color: 'snow',
      running: false,
      lastScore: 0
    };
    this.drawSnow = this.drawSnow.bind(this);
  }

  componentDidMount() {
    this.drawSnow();
  }

  drawSnow = () => {
    let canvas = document.getElementById("myCanvas");
    let ctx = canvas.getContext("2d");
    ctx.canvas.width  = window.innerWidth;
    ctx.canvas.height = window.innerHeight;

    let cw = canvas.width;
    let ch = canvas.height;

    const colors = [
      "violet", "indigo", "blue", "green", "yellow", "orange", "red"
    ];

    let x, y, color, i, random, radius;

    const fps = 1;

    const draw = () => {
      setTimeout(() => {
        requestAnimationFrame(draw);

        ctx.clearRect(0, 0, cw, ch);

        for (i=0; i<300; i++) {
          x = Math.random()*cw;
          y = Math.random()*ch;

          if (this.state.color === 'snow') {
            color = 'white'
          } else {
            random = Math.floor(Math.random() * colors.length);
            color = colors[random];
          }

          radius = Math.floor(Math.random() * 4);

          ctx.beginPath();
          ctx.arc(x,y,radius,0,Math.PI*2);
          ctx.fillStyle = color;
          ctx.globalAlpha = 1;
          ctx.fill();
        }
      }, 1000 / fps);
    };

    draw();
  };

  toggleColor = () => {
    if (this.state.color === 'snow') {
      this.setState({
        color: 'light'
      });
    } else {
      this.setState({
        color: 'snow'
      });
    }
  };

  startRunning = () => {
    if (this.state.running === false) {
      this.setState({
        running: true,
        score: 0
      });
    }
  };

  stopRunning = (score) => {
    this.setState({
      running: false,
      lastScore: score
    });
  };

  render () {
    return (
      <div className="App">
        <img src='santa.gif' alt="santa" style={{position: 'absolute', left: '20px', top: '40px'}}/>
        <button onClick={this.startRunning} style={{position: 'fixed', top: '70px', left: '80px'}}>START</button>
        {
          this.state.running && <Game stop={this.stopRunning}/>
        }
        <div style={{position: 'absolute', top: '300px', left: '30px', zIndex: '9999', color: 'white'}}>
          <h4>Collect 70 gifts in 60 seconds</h4>
          <p>Last score: <b>{this.state.lastScore}</b> <br/> jump (up arrow), slow down (left arrow), speed up (right arrow)</p>
        </div>

        <button onClick={this.toggleColor} style={{position: 'fixed', top: '10px', left: '10px'}}>TOGGLE</button>

        <header className="App-header">
          <img src='logo.png' className="App-logo" alt="logo" />
        </header>
      </div>
    );
  }
}

export default App;
