import React from 'react';
import {Santa, Gifts, Gaps} from './elements';


const style = {
  border: '1px solid #000000',
  backgroundColor: 'blue',
  position: 'absolute',
  left: '10px',
  top: '100px'
};


class Game extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      seconds: 0,
      start: Date.now()
    };

    this.drawGame = this.drawGame.bind(this);

    this.timerInterval = null;
    this.gameTimeOut = null;
    this.checkKeyPress = null;
    this.checkKeyRelease = null;
    this.canvas = null;
  }

  componentDidMount() {
    this.drawGame();
    this._isMounted = true;
  }

  componentWillUnmount() {
    if (this.timerInterval) clearInterval(this.timerInterval);
    if (this.gameTimeOut) clearTimeout(this.gameTimeOut);

    if (this.checkKeyPress) window.removeEventListener('keydown', this.checkKeyPress);
    if (this.checkKeyRelease) window.removeEventListener('keyup', this.checkKeyRelease);

    // if (this.canvas) {
    //   const ctx = this.canvas.getContext('2d');
    //   let cw=this.canvas.width;
    //   let ch=this.canvas.height;
    //   ctx.clearRect(0, 0, cw, ch);
    // }

    this._isMounted = false;
  }

  drawGame = () => {
    this.canvas = document.getElementById('santaCanvas');
    const ctx = this.canvas.getContext('2d');
    let cw = this.canvas.width;
    let ch = this.canvas.height;

    let santa = new Santa(this.canvas);
    let gifts = new Gifts(this.canvas);
    let gaps = new Gaps(this.canvas);

    let score = 0;

    const fps = 15;

    this.timerInterval = setInterval(() => {
      let delta = Date.now() - this.state.start; // milliseconds elapsed since start
      delta = Math.floor(delta / 1000); // in seconds

      if (this._isMounted)
        this.setState({
          seconds: delta
        });

      if (this.state.seconds >= 60) {
        this.props.stop(score);

        if (this._isMounted)
          this.setState({
            score: 0
          });
      }
    }, 1000);

    this.checkKeyPress = (e) => {
      const code = e.keyCode;

      switch (code) {
        case 37: // Left key
          gifts.reduceSpeed();
          gaps.reduceSpeed();
          break;
        case 38: // Up Key
          if (santa.jump === false)
            santa.jump = true;
          break;
        case 39: // Right Key
          gifts.increaseSpeed();
          gaps.increaseSpeed();
          break;
        default: console.log(code); //Everything else
      }
    };

    this.checkKeyRelease = (e) => {
      const code = e.keyCode;

      switch (code) {
        case 37: // Left key
          gifts.resetSpeed();
          gaps.resetSpeed();
          break;
        case 39: // Right Key
          gifts.resetSpeed();
          gaps.resetSpeed();
          break;
        default: console.log(code); //Everything else
      }
    };

    window.addEventListener('keydown', this.checkKeyPress,false);
    window.addEventListener('keyup', this.checkKeyRelease,false);

    const draw = () => {
      this.gameTimeOut = setTimeout(() => {
        requestAnimationFrame(draw);

        // clear the canvas
        ctx.clearRect(0, 0, cw, ch);

        // show score
        ctx.font = "20px Arial";
        ctx.fillStyle = 'white';
        ctx.fillText(`Score: ${score}`, 10, 50);

        // show timer
        ctx.font = "20px Arial";
        ctx.fillStyle = 'white';
        ctx.fillText(`Time: ${this.state.seconds}`, 10, 20);

        // create log
        ctx.fillStyle = 'brown';
        ctx.fillRect(0, 155, cw, 30);

        ctx.fillStyle = '#601415';
        ctx.fillRect(0, 155, cw, 12);

        // render santa
        santa.move();
        santa.render();

        // render gifts
        score += gifts.render(santa)

        // render gaps
        if (gaps.render(santa)) {
          santa.fall = true;
        }

      }, 1000 / fps);
    };

    draw();
  };

  render() {
    return (
      <canvas id="santaCanvas" height="300px" width="500px" style={style}>
        Your browser does not support the HTML canvas tag.
      </canvas>
    );
  }
}

export default Game;
